# LIS 4381

## Breanna Bush

### Assignment 1 Requirements:

*Sub-Heading:*

1. Ordered-list items
2. Screenshots of Applications
3. Bitbucket Repository Links

#### README.md file should include the following items:

* Bullet-list items
* Screenshot of AMPPS Installation
* Screenshot of Running Java Hello
* Screenshot of Running Android Studio - My First App
* Git Commands w/ Short Description
* Bitbucket Repo Links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creates a new local repository
2. git status - Lists all new or modified files to be committed
3. git add - Snapshots the file in preparation for versioning
4. git commit - Records file snapshots permanently in version history
5. git push - Uploads all local branch commits to GitHub
6. git pull - Downloads bookmark history and incorporates changes
7. git diff - Shows file differences not yet staged


#### Assignment Screenshots:

*Screenshot of AMPPS running*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/javaHello.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/myFirstApp.png)


#### Tutorial Link and Assignment Link:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/brb14d/bitbucketstationlocations/ "Bitbucket Station Locations")

*Assignment: Class repository:*
[A1 My Class Repository Link](https://bitbucket.org/brb14d/lis4381/ "Class Repository")
