# LIS 4381

## Breanna Bush

### Assignment 3 Requirements:

*Create a Mobile Ticket Price Calculation App using Android Studio*

1. Screenshots of User Interfaces
2. Bitbucket Repo Links

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of Running Application's First User Interface
* Screenshot of Running Application's Second User Interface
* Link to a3.mwb
* Link to a3.sql
* Bitbucket Repo Links


#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](img/mySQL.png)

*Screenshot of Running Application First User Interface*:

![First User Interface Screenshot](img/firstUI.png)

*Screenshot of Running Application Second User Interface*:

![Second User Interface Screenshot](img/secondUI.png)


#### Tutorial Link and Assignment Link:

*Links to the files:*
[A3 MWB Link](https://bitbucket.org/brb14d/lis4381/a3 )

[A3 SQL Link](https://bitbucket.org/brb14d/lis4381/a3 )

*Assignment: Class repository:*
[A3 My Class Repository Link](https://bitbucket.org/brb14d/lis4381/ "Class Repository")
