> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4381 - Mobile Web Application Development

## Breanna Bush

### LIS4381 Requirements:

*Student files for mobile-first web application development!*
*Course Work Links:*

1. [a1 README.md](a1/README.md "My A1 README.md file")
    * Installed AMPPS
    * Installed JDK and Java SE
    * Installed Android Studio and created My First App
    * Provided screenshots of all installations
    * Created Bitbucket repo
    * Completed Bitbucket tutorial (bitbucketstationlocations)
    * Provide git command descriptions




2. [a2 README.md](a2/README.md "My A2 README.md file")
    * Create First Interface for Healthy Recipe App
    * Create Second Interface for Healthy Recipe App
    * Push all deliverables to bitbucket




3. [a3 README.md](a3/README.md "My A3 README.md file")
    * Created ERD Model 
    * Forward Egineer ERD Model
    * Provided link to mwb & sql files
    * Provided Screenshots of ERD Model
    * Created Ticket Price Calculation (My Event) App
    * Provided Screenshots of My Event App




4. [p1 README.md](p1/README.md "My P1 README.md file")
    * Create First Interface for My Business Card App
    * Create Second Interface for My Business Card App
    * Push all deliverables to bitbucket




5. [a4 README.md](a4/README.md "My A4 README.md file")
    * Provided screenshot of Main Page
    * Provided screenshot of Failed Validation
    * Provided screenshot of Passed Validation
    * Provided link local LIS4381 Web App
    * Push all deliverables to bitbucket




6. [a5 README.md](a5/README.md "My A5 README.md file")
    * Provided screenshot of Main Page
    * Provided screenshot of Error Page
    * Provided link local LIS4381 Web App
    * Push all deliverables to bitbucket




7. [p2 README.md](p2/README.md "My P2 README.md file")
    * Provided screenshot of Main Page
    * Provided screenshot of Error Page
    * Provided link local LIS4381 Web App
    * Provided link to RSS Feed
    * Push all deliverables to bitbucket