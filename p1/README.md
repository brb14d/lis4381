# LIS 4381

## Breanna Bush

### Project 1 Requirements:

*Create a Mobile Business Card App using Android Studio*

1. Screenshots of User Interfaces
2. Bitbucket Repo Links

#### README.md file should include the following items:

* Screenshot of running application’s first user interface
* Screenshot of running application’s second user interface
* Bitbucket Repo Links


#### Assignment Screenshots:

*Screenshot of Running Application First User Interface*:

![First User Interface Screenshot](img/firstUI.png)

*Screenshot of Running Application Second User Interface*:

![Second User Interface Screenshot](img/secondUI.png)


#### Tutorial Link and Assignment Link:

*Links to the files:*

*Assignment: Class repository:*
[P1 My Class Repository Link](https://bitbucket.org/brb14d/lis4381/ "Class Repository")


