# LIS 4381

## Breanna Bush

### Assignment 2 Requirements:

*Create a mobile recipe app using Android Studio.*

1. Screenshots of User Interfaces
2. Bitbucket Repository Links

#### README.md file should include the following items:

* Screenshot of Running Application First User Interface
* Screenshot of Running Application Second User Interface
* Bitbucket Repo Links


#### Assignment Screenshots:

*Screenshot of Running Application First User Interface*:

![First User Interface Screenshot](img/firstUI.png)

*Screenshot of Running Application Second User Interface*:

![Second User Interface Screenshot](img/secondUI.png)


#### Tutorial Link and Assignment Link:

*Assignment: Class repository:*
[A2 My Class Repository Link](https://bitbucket.org/brb14d/lis4381/ "Class Repository")
